/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversortemp;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

/**
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label celsius;
    
    @FXML
    private Label kelvin;
    
    @FXML
    private Label farenheit;
    
    @FXML
    private Button converter;
    
    @FXML
    private TextArea recebeC;
    
    @FXML
    private TextArea recebeK;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        farenheit.setText("Hello World!");
    }
    
    @FXML
    private void metodoQualquer(){
        if("".equals(recebeK.getText())){
            if(!("".equals(recebeC.getText()))){
                int cel = Integer.parseInt(recebeC.getText()),
                    kel = Integer.parseInt(recebeC.getText()) + 273,
                    far = (18*(Integer.parseInt(recebeC.getText())))/10 + 32;
                
                celsius.setText(""+cel);
                kelvin.setText(""+kel);
                farenheit.setText(""+far);
            }
        }
        
        else{
            int cel = Integer.parseInt(recebeK.getText()) - 273,
                kel = Integer.parseInt(recebeK.getText()),
                far = (cel*18)/10 + 32;
                
                celsius.setText(""+cel);
                kelvin.setText(""+kel);
                farenheit.setText(""+far);
        }
    }
    
    @FXML
    private void apagaK(){
        recebeK.setText("");
    }
    
    
    @FXML
    private void apagaC(){
        recebeC.setText("");
    }        
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
